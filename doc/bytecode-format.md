# Bytecode format
This document describes the bytecode format as used by the abc-interpreter and this code generator. This document forms
the basis for bytecode parser. This information is only applies to 64 bit systems. This code generator will not support
32 bit systems in the foreseeable future.

```
ABCFile
	= ABC_MAGIC_NUMBER(32)
	  HEADER
	  -- code_size --
	  | CODE
	  --
	  -- string_size --
	  | STRING_METADATA
	  --
	  -- data_size --
	  | DATA
	  --
	  -- symbol_table_size --
	  | SYMBOL_TYPE
	  --
	  -- code_reloc_size (in entries) --
	  | CODE_RELOCATIONS
	  --
	  -- data_reloc_size (in entries) --
	  | DATA_RELOCATIONS
	  --

HEADER
	= header_length(32)
	  -- header_length --
	  | ABC_VERSION(32)
	  | code_size(32)
	  | words_in_string(32)
	  | strings_size(32)
	  | data_size(32)
	  | nr_of_symbols(32)
	  | symbol_table_size(32)
	  | code_reloc_size(32)
	  | data_reloc_size(32)
	  | start_symbol_id(32)
	  | RESERVED
	  -- header_length --

CODE
	= INSTRUCTION+

STRING_METADATA:
	= UNUSED(32)

INSTRUCTION
	= ...

DATA
	// Can simply be stored in module-wide array
	= data(64)*

SYMBOL_TABLE
	= SYMBOL_ENTRY[nr_of_symbols]

/*
 * The offset is shifted two bits to the left, and the least significant bit is set when the
 * offset is into the data section or cleared when it is into the code section.
 *
 * ERIN: In the grammar we represent this as a 30 bit offset and a 2 bit target (code or data).
 *
 * If it is an offset into the code section, it is assumed that each instruction and argument in the code section
 * takes one “space”. For example, if there is a buildI instruction at offset 0, the next instruction will be at
 * offset 2, because buildI has one argument (although the buildI instruction takes 10 bytes in total). This is
 * useful because at runtime the interpreter uses one word for each instruction and instruction argument, which is
 * done for performance reasons.
 *
 * Symbols are sorted ascending by name. Names are unique, with the exception of the empty string. Symbols with the
 * empty string as name are only used for relocations and appear after all symbols with names.
 *
 * [Staps 2019]
 */
SYMBOL_ENTRY
	= offset(30)
	  EMPTY(1)
	  target(1)
	  name(..)
	  '\0'

CODE_RELOCATIONS
	= CODE_RELOCATION*

CODE_RELOCATION
	= offset(32)
	  symbol_index(32)

DATA_RELOCATIONS
	= DATA_RELOCATION*

DATA_RELOCATION
	= offset(32)
	  symbol_index(32)

ABC_MAGIC_NUMBER
	= 0x2a434241 (ABC*)

ABC_VERSION
	= 18(32)
```

## References
[Staps 2019]: Lazy Interworking of Compiled and Interpreted Code for Sandboxing and Distributed Systems
