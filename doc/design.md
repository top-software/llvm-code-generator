# Module layout
## `parse::*`
Contains all modules related to parsing the initial bytecode. Does not perform any transformations.

## `ast::*`
Contains all parts of the AST that are shared among passes.
### `ast::instruction`
A large enum of all ABC instructions that can be interpreted by the ABC interpreter.

## `pass::*`
Contains the modules for every pass. A pass is defined as anything that performs transformations on the AST.

### `pass::relocation`
The relocation pass: `ParsedProgram -> RelocatedProgram`, resolves the relocations in both the code and data segments.

### `pass::label`
Adds the labels to the AST: `RelocatedProgram -> LabelledProgram`, removes the symbol table, creates strings for labels
that have been removed from the bytecode.

### `pass::basic_block`
Identifies and creates basic blocks in the AST: `LabelledProgram -> BasicBlockProgram`.

### `pass::llvmify`
Converts the AST to one that abides to the general layout of LLVM (does not generate LLVM-IR): `BasicBlockProgram ->
LLVMifiedProgram`, for example removes the fall through present in ABC but illegal in LLVM-IR.

## `llvm::*`
Generates the LLVM code.
