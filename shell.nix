with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "cllm-env";
  nativeBuildInputs = [
    rustc
    cargo
    rust-analyzer

    pkgconfig
  ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;
}
