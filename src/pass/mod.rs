//! This module exports all passes. Since all passes are selfcontained, should only export
//! submodules.

pub(crate) mod relocation;
