//! Performs the relocations

use crate::ast::instruction::Instruction;
use crate::ast::instruction::Instruction::*;
use crate::ast::instruction::LabelOffset;
use crate::ast::symbol_table::SymbolTable;
use crate::parse;
use crate::parse::ParsedProgram;

/// The relocated program; no longer contains the code_relocations and data_relocations fields.
#[derive(Debug)]
pub(crate) struct RelocatedProgram {
    /// A `Vec` of Instructions, Labels and Arities.
    pub(crate) code: Code,
    /// A raw data segment.
    pub(crate) data: Vec<u64>,
    /// Matching symbols to their offsets in the code and data sections.
    pub(crate) symbol_table: SymbolTable,
}

// We have to keep the offset as it will be needed when adding labels
pub(crate) type Code = Vec<(Offset, parse::Line)>;

pub(crate) type Offset = u32;

/// Resolves all fixups in the relocation tables
pub(crate) fn relocate_program(parsed_program: ParsedProgram) -> Result<RelocatedProgram, String> {
    let mut code = parsed_program.code.clone();
    let mut data = parsed_program.data.clone();
    let mut reloc_iter = parsed_program.code_relocations.iter().peekable();

    // For every instruction we fetch what relocations we have to do for that segment. The other way around
    // would be nicer, but we can't directly index the code, meaning that we would end up with a O(n*m) algorithm.
    for (c_offset, c_line) in code.iter_mut() {
        match c_line {
            parse::Line::Instruction(i) => {
                let len = i.len();
                while let Some((r_offset, r_index)) = reloc_iter.peek() {
                    if *r_offset < *c_offset + len {
                        relocate_instruction(i, *r_offset - *c_offset, *r_index as u16)?;
                        reloc_iter.next();
                    } else {
                        break;
                    }
                }
            }
            parse::Line::Label(l) => {
                if let Some((r_offset, r_index)) = reloc_iter.peek() {
                    if r_offset == c_offset {
                        *l = *r_index as u16;
                        reloc_iter.next();
                    }
                }
            }
            // Relocations should never fall in Arities
            parse::Line::Arity(_) => {}
        }
    }

    for (r_offset, r_index) in parsed_program.data_relocations {
        // We only target x64 so this is fine
        // Ideally we would deal with converting to usize in the parser
        data[r_offset as usize] = r_index as u64;
    }

    Ok(RelocatedProgram {
        code,
        data,
        symbol_table: parsed_program.symbol_table,
    })
}

/// Relocates a single instruction
/// * `instr` - The instruction that is has a label that must be updated
/// * `relative_offset` - The offset of the offset that mast be changed 1 indexed (the instruction
/// itself is 0)
/// * `new_offset` - The relocated location
fn relocate_instruction(
    instr: &mut Instruction<LabelOffset>,
    relative_offset: u32,
    new_offset: LabelOffset,
) -> Result<(), String> {
    match (relative_offset, instr.clone()) {
        (1, add_empty_node2(_)) => *instr = add_empty_node2(new_offset),
        (1, add_empty_node3(_)) => *instr = add_empty_node3(new_offset),
        (1, add_empty_node4(_)) => *instr = add_empty_node4(new_offset),
        (1, add_empty_node5(_)) => *instr = add_empty_node5(new_offset),
        (1, add_empty_node6(_)) => *instr = add_empty_node6(new_offset),
        (1, add_empty_node7(_)) => *instr = add_empty_node7(new_offset),
        (1, add_empty_node8(_)) => *instr = add_empty_node8(new_offset),
        (1, add_empty_node9(_)) => *instr = add_empty_node9(new_offset),
        (1, add_empty_node10(_)) => *instr = add_empty_node10(new_offset),
        (1, add_empty_node11(_)) => *instr = add_empty_node11(new_offset),
        (1, add_empty_node12(_)) => *instr = add_empty_node12(new_offset),
        (1, add_empty_node13(_)) => *instr = add_empty_node13(new_offset),
        (1, add_empty_node14(_)) => *instr = add_empty_node14(new_offset),
        (1, add_empty_node15(_)) => *instr = add_empty_node15(new_offset),
        (1, add_empty_node16(_)) => *instr = add_empty_node16(new_offset),
        (1, add_empty_node17(_)) => *instr = add_empty_node17(new_offset),
        (1, add_empty_node18(_)) => *instr = add_empty_node18(new_offset),
        (1, add_empty_node19(_)) => *instr = add_empty_node19(new_offset),
        (1, add_empty_node20(_)) => *instr = add_empty_node20(new_offset),
        (1, add_empty_node21(_)) => *instr = add_empty_node21(new_offset),
        (1, add_empty_node22(_)) => *instr = add_empty_node22(new_offset),
        (1, add_empty_node23(_)) => *instr = add_empty_node23(new_offset),
        (1, add_empty_node24(_)) => *instr = add_empty_node24(new_offset),
        (1, add_empty_node25(_)) => *instr = add_empty_node25(new_offset),
        (1, add_empty_node26(_)) => *instr = add_empty_node26(new_offset),
        (1, add_empty_node27(_)) => *instr = add_empty_node27(new_offset),
        (1, add_empty_node28(_)) => *instr = add_empty_node28(new_offset),
        (1, add_empty_node29(_)) => *instr = add_empty_node29(new_offset),
        (1, add_empty_node30(_)) => *instr = add_empty_node30(new_offset),
        (1, add_empty_node31(_)) => *instr = add_empty_node31(new_offset),
        (1, add_empty_node32(_)) => *instr = add_empty_node32(new_offset),
        (2, build(si, _)) => *instr = build(si, new_offset),
        (1, build0(_)) => *instr = build0(new_offset),
        (1, build1(_)) => *instr = build1(new_offset),
        (1, build2(_)) => *instr = build2(new_offset),
        (1, build3(_)) => *instr = build3(new_offset),
        (1, build4(_)) => *instr = build4(new_offset),
        (1, buildAC(so)) => *instr = buildAC(so),
        (2, buildh(si, _)) => *instr = buildh(si, new_offset),
        (1, buildh0(_)) => *instr = buildh0(new_offset),
        (1, buildh1(_)) => *instr = buildh1(new_offset),
        (1, buildh2(_)) => *instr = buildh2(new_offset),
        (1, buildh3(_)) => *instr = buildh3(new_offset),
        (1, buildh4(_)) => *instr = buildh4(new_offset),
        (1, buildhr01(_)) => *instr = buildhr01(new_offset),
        (1, buildhr02(_)) => *instr = buildhr02(new_offset),
        (1, buildhr03(_)) => *instr = buildhr03(new_offset),
        (1, buildhr04(_)) => *instr = buildhr04(new_offset),
        (1, buildhr10(_)) => *instr = buildhr10(new_offset),
        (1, buildhr11(_)) => *instr = buildhr11(new_offset),
        (1, buildhr12(_)) => *instr = buildhr12(new_offset),
        (1, buildhr13(_)) => *instr = buildhr13(new_offset),
        (2, buildhr1b(si, _)) => *instr = buildhr1b(si, new_offset),
        (1, buildhr20(_)) => *instr = buildhr20(new_offset),
        (1, buildhr21(_)) => *instr = buildhr21(new_offset),
        (1, buildhr22(_)) => *instr = buildhr22(new_offset),
        (1, buildhr30(_)) => *instr = buildhr30(new_offset),
        (1, buildhr31(_)) => *instr = buildhr31(new_offset),
        (1, buildhr40(_)) => *instr = buildhr40(new_offset),
        (3, build_r(si1, si2, _, si3, si4)) => *instr = build_r(si1, si2, new_offset, si3, si4),
        (3, build_ra0(si1, si2, _)) => *instr = build_ra0(si1, si2, new_offset),
        (3, build_ra1(si1, si2, _, si3)) => *instr = build_ra1(si1, si2, new_offset, si3),
        (3, build_r0b(si1, si2, _)) => *instr = build_r0b(si1, si2, new_offset),
        (2, buildhr(si1, _, si2)) => *instr = buildhr(si1, new_offset, si2),
        (2, buildhra0(si, _)) => *instr = buildhra0(si, new_offset),
        (2, buildhra1(si, _)) => *instr = buildhra1(si, new_offset),
        (2, buildhr0b(si, _)) => *instr = buildhr0b(si, new_offset),
        (2, build_r01(si, _)) => *instr = build_r01(si, new_offset),
        (2, build_r02(si, _)) => *instr = build_r02(si, new_offset),
        (2, build_r03(si, _)) => *instr = build_r03(si, new_offset),
        (2, build_r04(si, _)) => *instr = build_r04(si, new_offset),
        (2, build_r10(si, _)) => *instr = build_r10(si, new_offset),
        (3, build_r11(si1, si2, _)) => *instr = build_r11(si1, si2, new_offset),
        (3, build_r12(si1, si2, _)) => *instr = build_r12(si1, si2, new_offset),
        (3, build_r13(si1, si2, _)) => *instr = build_r13(si1, si2, new_offset),
        (3, build_r1b(si1, si2, _, si3)) => *instr = build_r1b(si1, si2, new_offset, si3),
        (2, build_r20(si, _)) => *instr = build_r20(si, new_offset),
        (3, build_r21(si1, si2, _)) => *instr = build_r21(si1, si2, new_offset),
        (2, build_r30(si, _)) => *instr = build_r30(si, new_offset),
        (3, build_r31(si1, si2, _)) => *instr = build_r31(si1, si2, new_offset),
        (2, build_r40(si, _)) => *instr = build_r40(si, new_offset),
        (2, build_u(si1, _, si2)) => *instr = build_u(si1, new_offset, si2),
        (1, build_u01(_)) => *instr = build_u01(new_offset),
        (1, build_u02(_)) => *instr = build_u02(new_offset),
        (1, build_u03(_)) => *instr = build_u03(new_offset),
        (2, build_u0b(si, _)) => *instr = build_u0b(si, new_offset),
        (1, build_u11(_)) => *instr = build_u11(new_offset),
        (1, build_u12(_)) => *instr = build_u12(new_offset),
        (1, build_u13(_)) => *instr = build_u13(new_offset),
        (2, build_u1b(si, _)) => *instr = build_u1b(si, new_offset),
        (1, build_u21(_)) => *instr = build_u21(new_offset),
        (1, build_u22(_)) => *instr = build_u22(new_offset),
        (2, build_u2b(si, _)) => *instr = build_u2b(si, new_offset),
        (1, build_u31(_)) => *instr = build_u31(new_offset),
        (2, build_ua1(si, _)) => *instr = build_ua1(si, new_offset),
        (1, ccall(_, so)) => *instr = ccall(new_offset, so),
        (2, ccall(so, _)) => *instr = ccall(so, new_offset),
        (3, create_array_r(si1, si2, _)) => *instr = create_array_r(si1, si2, new_offset),
        (3, create_array_r_(si1, si2, _)) => *instr = create_array_r_(si1, si2, new_offset),
        (2, create_array_r_a(si, _)) => *instr = create_array_r_a(si, new_offset),
        (2, create_array_r_b(si, _)) => *instr = create_array_r_b(si, new_offset),
        (1, eqAC_a(_)) => *instr = eqAC_a(new_offset),
        (1, eqD_b(_)) => *instr = eqD_b(new_offset),
        (2, eq_desc(si, _)) => *instr = eq_desc(si, new_offset),
        (1, eq_desc_b(_)) => *instr = eq_desc_b(new_offset),
        (2, eq_nulldesc(si, _)) => *instr = eq_nulldesc(si, new_offset),
        (3, fill(si1, si2, _)) => *instr = fill(si1, si2, new_offset),
        (2, fill0(si, _)) => *instr = fill0(si, new_offset),
        (2, fill1_r0111(si, _)) => *instr = fill1_r0111(si, new_offset),
        (2, fill1_r02101(si, _)) => *instr = fill1_r02101(si, new_offset),
        (2, fill1_r02110(si, _)) => *instr = fill1_r02110(si, new_offset),
        (2, fill1_r02111(si, _)) => *instr = fill1_r02111(si, new_offset),
        (2, fill1_r11101(si, _)) => *instr = fill1_r11101(si, new_offset),
        (2, fill1_r11111(si, _)) => *instr = fill1_r11111(si, new_offset),
        (2, fill1_r20111(si, _)) => *instr = fill1_r20111(si, new_offset),
        (2, fill1(si, _)) => *instr = fill1(si, new_offset),
        (2, fill1101(si, _)) => *instr = fill1101(si, new_offset),
        (2, fill2(si, _)) => *instr = fill2(si, new_offset),
        (2, fill2_r10(si1, _, si2, bv, caflo)) => {
            *instr = fill2_r10(si1, new_offset, si2, bv, caflo)
        }
        (5, fill2_r10(si1, l, si2, bv, _)) => *instr = fill2_r10(si1, l, si2, bv, new_offset),
        (2, fill2_r11(si1, _, si2, bv, caflo)) => {
            *instr = fill2_r11(si1, new_offset, si2, bv, caflo)
        }
        (5, fill2_r11(si1, l, si2, bv, _)) => *instr = fill2_r11(si1, l, si2, bv, new_offset),
        (2, fill3(si, _)) => *instr = fill3(si, new_offset),
        (2, fill3a10(si, _)) => *instr = fill3a10(si, new_offset),
        (2, fill3a11(si1, _, si2)) => *instr = fill3a11(si1, new_offset, si2),
        (2, fill3a12(si1, _, si2, si3)) => *instr = fill3a12(si1, new_offset, si2, si3),
        (2, fill3aaab13(si1, _, si2, si3, si4)) => {
            *instr = fill3aaab13(si1, new_offset, si2, si3, si4)
        }
        (2, fill3_r(si1, _, si2, si3, bv, caflo)) => {
            *instr = fill3_r(si1, new_offset, si2, si3, bv, caflo)
        }
        (6, fill3_r(si1, l, si2, si3, bv, _)) => *instr = fill3_r(si1, l, si2, si3, bv, new_offset),
        (2, fill3_r01a(si1, _, si2)) => *instr = fill3_r01a(si1, new_offset, si2),
        (2, fill3_r01b(si1, _, si2)) => *instr = fill3_r01b(si1, new_offset, si2),
        (2, fill4(si1, _)) => *instr = fill4(si1, new_offset),
        (1, fillcaf(_, si1, si2)) => *instr = fillcaf(new_offset, si1, si2),
        (3, fillh(si1, si2, _)) => *instr = fillh(si1, si2, new_offset),
        (2, fillh0(si, _)) => *instr = fillh0(si, new_offset),
        (2, fillh1(si, _)) => *instr = fillh1(si, new_offset),
        (2, fillh2(si, _)) => *instr = fillh2(si, new_offset),
        (2, fillh3(si, _)) => *instr = fillh3(si, new_offset),
        (2, fillh4(si, _)) => *instr = fillh4(si, new_offset),
        (4, fill_r(si1, si2, si3, _, si4, si5)) => {
            *instr = fill_r(si1, si2, si3, new_offset, si4, si5)
        }
        (3, fill_r01(si1, si2, _)) => *instr = fill_r01(si1, si2, new_offset),
        (3, fill_r02(si1, si2, _)) => *instr = fill_r02(si1, si2, new_offset),
        (3, fill_r03(si1, si2, _)) => *instr = fill_r03(si1, si2, new_offset),
        (3, fill_r0b(si1, si2, _, si3)) => *instr = fill_r0b(si1, si2, new_offset, si3),
        (3, fill_r10(si1, si2, _)) => *instr = fill_r10(si1, si2, new_offset),
        (4, fill_r11(si1, si2, si3, _)) => *instr = fill_r11(si1, si2, si3, new_offset),
        (4, fill_r12(si1, si2, si3, _)) => *instr = fill_r12(si1, si2, si3, new_offset),
        (4, fill_r13(si1, si2, si3, _)) => *instr = fill_r13(si1, si2, si3, new_offset),
        (4, fill_r1b(si1, si2, si3, _, si4)) => *instr = fill_r1b(si1, si2, si3, new_offset, si4),
        (3, fill_r20(si1, si2, _)) => *instr = fill_r20(si1, si2, new_offset),
        (4, fill_r21(si1, si2, si3, _)) => *instr = fill_r21(si1, si2, si3, new_offset),
        (4, fill_r22(si1, si2, si3, _)) => *instr = fill_r22(si1, si2, si3, new_offset),
        (3, fill_r30(si1, si2, _)) => *instr = fill_r30(si1, si2, new_offset),
        (4, fill_r31(si1, si2, si3, _)) => *instr = fill_r31(si1, si2, si3, new_offset),
        (3, fill_r40(si1, si2, _)) => *instr = fill_r40(si1, si2, new_offset),
        (4, fill_ra0(si1, si2, si3, _)) => *instr = fill_ra0(si1, si2, si3, new_offset),
        (4, fill_ra1(si1, si2, si3, _, si4)) => *instr = fill_ra1(si1, si2, si3, new_offset, si4),
        (3, fill_u(si1, si2, _, si3)) => *instr = fill_u(si1, si2, new_offset, si3),
        (1, jmp(_)) => *instr = jmp(new_offset),
        (1, jmpD_ab(_, l1, l2)) => *instr = jmpD_ab(new_offset, l1, l2),
        (2, jmpD_ab(l1, _, l2)) => *instr = jmpD_ab(l1, new_offset, l2),
        (3, jmpD_ab(l1, l2, _)) => *instr = jmpD_ab(l1, l2, new_offset),
        (1, jmpD_ae(_, l1, l2)) => *instr = jmpD_ae(new_offset, l1, l2),
        (2, jmpD_ae(l1, _, l2)) => *instr = jmpD_ae(l1, new_offset, l2),
        (3, jmpD_ae(l1, l2, _)) => *instr = jmpD_ae(l1, l2, new_offset),
        (1, jmpD_be(_, l1, l2)) => *instr = jmpD_be(new_offset, l1, l2),
        (2, jmpD_be(l1, _, l2)) => *instr = jmpD_be(l1, new_offset, l2),
        (3, jmpD_be(l1, l2, _)) => *instr = jmpD_be(l1, l2, new_offset),
        (1, jmp_false(_)) => *instr = jmp_false(new_offset),
        (1, jmp_true(_)) => *instr = jmp_true(new_offset),
        (1, jsr(_)) => *instr = jsr(new_offset),
        (1, print(_)) => *instr = print(new_offset),
        (1, pushD(_)) => *instr = pushD(new_offset),
        (2, push_node(si, _)) => *instr = push_node(si, new_offset),
        (1, push_node0(_)) => *instr = push_node0(new_offset),
        (1, push_node1(_)) => *instr = push_node1(new_offset),
        (1, push_node2(_)) => *instr = push_node2(new_offset),
        (1, push_node3(_)) => *instr = push_node3(new_offset),
        (1, push_node4(_)) => *instr = push_node4(new_offset),
        (2, push_node_u(si1, _, si2)) => *instr = push_node_u(si1, new_offset, si2),
        (1, push_node_u01(_)) => *instr = push_node_u01(new_offset),
        (1, push_node_u02(_)) => *instr = push_node_u02(new_offset),
        (1, push_node_u03(_)) => *instr = push_node_u03(new_offset),
        (2, push_node_u0b(si, _)) => *instr = push_node_u0b(si, new_offset),
        (1, push_node_u11(_)) => *instr = push_node_u11(new_offset),
        (1, push_node_u12(_)) => *instr = push_node_u12(new_offset),
        (1, push_node_u13(_)) => *instr = push_node_u13(new_offset),
        (2, push_node_u1b(si, _)) => *instr = push_node_u1b(si, new_offset),
        (1, push_node_u21(_)) => *instr = push_node_u21(new_offset),
        (1, push_node_u22(_)) => *instr = push_node_u22(new_offset),
        (1, push_node_u31(_)) => *instr = push_node_u31(new_offset),
        (2, push_node_ua1(si, _)) => *instr = push_node_ua1(si, new_offset),
        (1, buildh0_dup_a(_, si)) => *instr = buildh0_dup_a(new_offset, si),
        (1, buildh0_dup2_a(_, si)) => *instr = buildh0_dup2_a(new_offset, si),
        (1, buildh0_dup3_a(_, si)) => *instr = buildh0_dup3_a(new_offset, si),
        (1, buildh0_put_a(_, si)) => *instr = buildh0_put_a(new_offset, si),
        (1, buildh0_put_a_jsr(_, si, l)) => *instr = buildh0_put_a_jsr(new_offset, si, l),
        (3, buildh0_put_a_jsr(l, si, _)) => *instr = buildh0_put_a_jsr(l, si, new_offset),
        (3, buildho2(si1, si2, _)) => *instr = buildho2(si1, si2, new_offset),
        (2, buildo1(si, _)) => *instr = buildo1(si, new_offset),
        (3, buildo2(si1, si2, _)) => *instr = buildo2(si1, si2, new_offset),
        (2, jmp_b_false(si, _)) => *instr = jmp_b_false(si, new_offset),
        (3, jmp_eqACio(si, so, _)) => *instr = jmp_eqACio(si, so, new_offset),
        (3, jmp_eqC_b(si, c, _)) => *instr = jmp_eqC_b(si, c, new_offset),
        (3, jmp_eqC_b2(si, c1, _, c2, l)) => *instr = jmp_eqC_b2(si, c1, new_offset, c2, l),
        (5, jmp_eqC_b2(si, c1, l, c2, _)) => *instr = jmp_eqC_b2(si, c1, l, c2, new_offset),
        (2, jmp_eqCc(c, _)) => *instr = jmp_eqCc(c, new_offset),
        (1, jmp_eqD_b(_, l)) => *instr = jmp_eqD_b(new_offset, l),
        (2, jmp_eqD_b(l, _)) => *instr = jmp_eqD_b(l, new_offset),
        (1, jmp_eqD_b2(_, l1, l2, l3)) => *instr = jmp_eqD_b2(new_offset, l1, l2, l3),
        (2, jmp_eqD_b2(l1, _, l2, l3)) => *instr = jmp_eqD_b2(l1, new_offset, l2, l3),
        (3, jmp_eqD_b2(l1, l2, _, l3)) => *instr = jmp_eqD_b2(l1, l2, new_offset, l3),
        (4, jmp_eqD_b2(l1, l2, l3, _)) => *instr = jmp_eqD_b2(l1, l2, l3, new_offset),
        (1, jmp_eqI(_)) => *instr = jmp_eqI(new_offset),
        (3, jmp_eqI_b(si, i, _)) => *instr = jmp_eqI_b(si, i, new_offset),
        (3, jmp_eqI_b2(si, i1, _, i2, l)) => *instr = jmp_eqI_b2(si, i1, new_offset, i2, l),
        (5, jmp_eqI_b2(si, i1, l, i2, _)) => *instr = jmp_eqI_b2(si, i1, l, i2, new_offset),
        (2, jmp_eqIi(i, _)) => *instr = jmp_eqIi(i, new_offset),
        (2, jmp_eq_desc(si, _, l)) => *instr = jmp_eq_desc(si, new_offset, l),
        (3, jmp_eq_desc(si, l, _)) => *instr = jmp_eq_desc(si, l, new_offset),
        (1, jmp_geI(_)) => *instr = jmp_geI(new_offset),
        (1, jmp_ltI(_)) => *instr = jmp_ltI(new_offset),
        (3, jmp_neC_b(si, c, _)) => *instr = jmp_neC_b(si, c, new_offset),
        (2, jmp_neCc(c, _)) => *instr = jmp_neCc(c, new_offset),
        (1, jmp_neI(_)) => *instr = jmp_neI(new_offset),
        (3, jmp_neI_b(si, i, _)) => *instr = jmp_neI_b(si, i, new_offset),
        (2, jmp_neIi(i, _)) => *instr = jmp_neIi(i, new_offset),
        (2, jmp_ne_desc(si, _, l)) => *instr = jmp_ne_desc(si, new_offset, l),
        (3, jmp_ne_desc(si, l, _)) => *instr = jmp_ne_desc(si, l, new_offset),
        (2, jmp_o_geI(si, _)) => *instr = jmp_o_geI(si, new_offset),
        (3, jmp_o_geI_arraysize_a(si1, si2, _)) => {
            *instr = jmp_o_geI_arraysize_a(si1, si2, new_offset)
        }
        (2, pop_a_jmp(si, _)) => *instr = pop_a_jmp(si, new_offset),
        (2, pop_a_jsr(si, _)) => *instr = pop_a_jsr(si, new_offset),
        (2, pop_b_jmp(si, _)) => *instr = pop_b_jmp(si, new_offset),
        (2, pop_b_jsr(si, _)) => *instr = pop_b_jsr(si, new_offset),
        (2, pushD_a_jmp_eqD_b2(si, _, l1, l2, l3)) => {
            *instr = pushD_a_jmp_eqD_b2(si, new_offset, l1, l2, l3)
        }
        (3, pushD_a_jmp_eqD_b2(si, l1, _, l2, l3)) => {
            *instr = pushD_a_jmp_eqD_b2(si, l1, new_offset, l2, l3)
        }
        (4, pushD_a_jmp_eqD_b2(si, l1, l2, _, l3)) => {
            *instr = pushD_a_jmp_eqD_b2(si, l1, l2, new_offset, l3)
        }
        (5, pushD_a_jmp_eqD_b2(si, l1, l2, l3, _)) => {
            *instr = pushD_a_jmp_eqD_b2(si, l1, l2, l3, new_offset)
        }
        (2, push_a_jsr(si, _)) => *instr = push_a_jsr(si, new_offset),
        (2, push_b_jsr(si, _)) => *instr = push_b_jsr(si, new_offset),
        (2, put_a_jmp(si, _)) => *instr = put_a_jmp(si, new_offset),
        (2, put_b_jmp(si, _)) => *instr = put_b_jmp(si, new_offset),
        (2, buildR_32(r, _)) => *instr = buildR_32(r, new_offset),
        (3, eqR_b_32(si, r, _)) => *instr = eqR_b_32(si, r, new_offset),
        (2, pushR_32(r, _)) => *instr = pushR_32(r, new_offset),
        _ => return Err(String::from("No label found at offset")),
    }
    Ok(())
}
