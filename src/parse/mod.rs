//! This module exposes all functions required to parse the binary format into a
//! naïve AST. This AST then transformed in later steps. For an overview of the
//! bytecode format see [bytecode-format.md](deps/bytecode-format.md). For a more
//! detailed overview see *Lazy Interworking of Compiled and Interpreted Code for
//! Sandboxing and Distributed System* by Camil Staps.

use crate::ast::instruction::{Arity, Instruction, LabelOffset};
use crate::ast::symbol_table::{Segment, SymbolTable, SymbolTableEntry};
use instruction::*;
use nom::{
    bytes::complete::{tag, take, take_until},
    multi::count,
    number::complete::{le_u16, le_u32, le_u64},
    IResult,
};

mod instruction;

/// All bytecode values must start with the magic ABC number.
const ABC_MAGIC: &[u8] = b"ABC*";

/// The ABC_VERSION stored in a slice of u8 to work with nom's tag function.
/// The bytecode is in a little endian format, hence the "reversed" order.
const ABC_VERSION: &[u8] = &[18, 0, 0, 0];

/// The final parsed result is a naïve representation of the program.
#[derive(Debug)]
pub(crate) struct ParsedProgram {
    /// A `Vec` of Instructions, Labels and Arities.
    pub(crate) code: Code,
    /// A raw data segment.
    pub(crate) data: Vec<u64>,
    /// Matching symbols to their offsets in the code and data sections.
    pub(crate) symbol_table: SymbolTable,
    /// Fixups in the code segment.
    pub(crate) code_relocations: CodeRelocations,
    /// Fixups in the data segment.
    pub(crate) data_relocations: DataRelocations,
}

#[derive(Debug)]
pub(crate) struct Header {
    length: u32,
    code_size: u32,
    words_in_string: u32,
    strings_size: u32,
    data_size: u32,
    nr_of_symbols: u32,
    symbol_table_size: u32,
    code_reloc_size: u32,
    data_reloc_size: u32,
    start_symbol_id: u32,
}

/// Every instruction, label and arity will be stored in this vector. The offset denotes the number of elements this
/// line is removed from the beginning of the code segment. This offset is needed such that we can perform slight code
/// transformations (like removing the annotation hints) without making relocating impossible.
pub(crate) type Code = Vec<(Offset, Line)>;

#[derive(Debug, Clone)]
pub(crate) enum Line {
    Instruction(Instruction<LabelOffset>),
    Label(LabelOffset),
    Arity(Arity),
}

pub(crate) type CodeRelocations = Vec<(Offset, Index)>;
pub(crate) type DataRelocations = Vec<(Offset, Index)>;

pub(crate) type Index = u32;
pub(crate) type Offset = u32;

/// Parse a single relocation/fixup.
fn parse_relocation(input: &[u8]) -> IResult<&[u8], (Offset, Index)> {
    let (input, offset) = le_u32(input)?;
    let (input, index) = le_u32(input)?;
    Ok((input, (index, offset)))
}

/// Given the number of entries in the following relocation table, parses that many relocations.
fn parse_relocations(mut input: &[u8], entries: u32) -> IResult<&[u8], Vec<(Offset, Index)>> {
    let mut map = Vec::with_capacity(entries as usize);
    for _ in 0..entries {
        let (tmp_input, (offset, index)) = parse_relocation(input)?;
        input = tmp_input;
        map.push((index, offset));
    }
    map.sort_unstable();
    Ok((input, map))
}

/// Parse the ABC header.
fn parse_header(input: &[u8]) -> IResult<&[u8], Header> {
    let (input, _) = tag(ABC_MAGIC)(input)?;
    let (input, length) = le_u32(input)?;
    let (input, _) = tag(ABC_VERSION)(input)?;
    let (input, code_size) = le_u32(input)?;
    let (input, words_in_string) = le_u32(input)?;
    let (input, strings_size) = le_u32(input)?;
    let (input, data_size) = le_u32(input)?;
    let (input, nr_of_symbols) = le_u32(input)?;
    let (input, symbol_table_size) = le_u32(input)?;
    let (input, code_reloc_size) = le_u32(input)?;
    let (input, data_reloc_size) = le_u32(input)?;
    let (input, start_symbol_id) = le_u32(input)?;
    // nom nom nom the remaining reserved spaces should almost always be 0 but this is not guaranteed
    let (input, _) = take(length - 10 * 4)(input)?;
    Ok((
        input,
        Header {
            length,
            code_size,
            words_in_string,
            strings_size,
            data_size,
            nr_of_symbols,
            symbol_table_size,
            code_reloc_size,
            data_reloc_size,
            start_symbol_id,
        },
    ))
}

/// Parse the code segment. The code segment is a mix of Instructions, Labels and
/// Arities, with the latter to being hinted at by the data hints. This function
/// distinguishes by first parsing a u32 and then matching on it. If the number
/// is below the number of instructions, it is an instructions. Otherwise it was
/// a data hint, which we parse accordingly.
fn parse_code(input: &[u8], code_size: u32) -> IResult<&[u8], Code> {
    let mut parsed_code_size: u32 = 0;
    let mut mut_input = input;
    let mut code = Vec::new();

    while parsed_code_size < code_size {
        let (input, instruction_int) = le_u16(mut_input)?;
        match instruction_int {
            0..=836 => {
                let (input, (instruction_size, instruction)) =
                    parse_instruction(instruction_int, input)?;
                code.push((parsed_code_size, Line::Instruction(instruction)));
                // Add to parsed code size only after pushing
                parsed_code_size += instruction_size;
                mut_input = input;
            }
            837 => {
                //IIIla
                // Annotation hints are considered part of the code segment
                parsed_code_size += 1;
                let (input, (instruction_size, instruction)) = parse_next_instruction(input)?;
                code.push((parsed_code_size, Line::Instruction(instruction)));
                parsed_code_size += instruction_size;
                let (input, (instruction_size, instruction)) = parse_next_instruction(input)?;
                code.push((parsed_code_size, Line::Instruction(instruction)));
                parsed_code_size += instruction_size;
                let (input, (instruction_size, instruction)) = parse_next_instruction(input)?;
                code.push((parsed_code_size, Line::Instruction(instruction)));
                parsed_code_size += instruction_size;
                let (input, label_offset) = le_u16(input)?;
                code.push((parsed_code_size, Line::Label(label_offset)));
                parsed_code_size += 1;
                let (input, arity) = le_u16(input)?;
                code.push((parsed_code_size, Line::Arity(arity)));
                parsed_code_size += 1;
                mut_input = input;
            }
            838 => {
                //IIl
                parsed_code_size += 1;
                let (input, (instruction_size, instruction)) = parse_next_instruction(input)?;
                code.push((parsed_code_size, Line::Instruction(instruction)));
                parsed_code_size += instruction_size;
                let (input, (instruction_size, instruction)) = parse_next_instruction(input)?;
                code.push((parsed_code_size, Line::Instruction(instruction)));
                parsed_code_size += instruction_size;
                let (input, label_offset) = le_u16(input)?;
                code.push((parsed_code_size, Line::Label(label_offset)));
                parsed_code_size += 1;
                mut_input = input;
            }
            839 => {
                //IlI
                parsed_code_size += 1;
                let (input, (instruction_size, instruction)) = parse_next_instruction(input)?;
                code.push((parsed_code_size, Line::Instruction(instruction)));
                parsed_code_size += instruction_size;
                let (input, label_offset) = le_u16(input)?;
                code.push((parsed_code_size, Line::Label(label_offset)));
                parsed_code_size += 1;
                let (input, (instruction_size, instruction)) = parse_next_instruction(input)?;
                code.push((parsed_code_size, Line::Instruction(instruction)));
                parsed_code_size += instruction_size;
                mut_input = input;
            }
            840 => {
                //IlIla
                parsed_code_size += 1;
                let (input, (instruction_size, instruction)) = parse_next_instruction(input)?;
                code.push((parsed_code_size, Line::Instruction(instruction)));
                parsed_code_size += instruction_size;
                let (input, label_offset) = le_u16(input)?;
                code.push((parsed_code_size, Line::Label(label_offset)));
                parsed_code_size += 1;
                let (input, (instruction_size, instruction)) = parse_next_instruction(input)?;
                code.push((parsed_code_size, Line::Instruction(instruction)));
                parsed_code_size += instruction_size;
                let (input, label_offset) = le_u16(input)?;
                code.push((parsed_code_size, Line::Label(label_offset)));
                parsed_code_size += 1;
                let (input, arity) = le_u16(input)?;
                code.push((parsed_code_size, Line::Arity(arity)));
                parsed_code_size += 1;
                mut_input = input;
            }
            841 => {
                //lIlI
                parsed_code_size += 1;
                let (input, label_offset) = le_u16(input)?;
                code.push((parsed_code_size, Line::Label(label_offset)));
                parsed_code_size += 1;
                let (input, (instruction_size, instruction)) = parse_next_instruction(input)?;
                code.push((parsed_code_size, Line::Instruction(instruction)));
                parsed_code_size += instruction_size;
                let (input, label_offset) = le_u16(input)?;
                code.push((parsed_code_size, Line::Label(label_offset)));
                parsed_code_size += 1;
                let (input, (instruction_size, instruction)) = parse_next_instruction(input)?;
                code.push((parsed_code_size, Line::Instruction(instruction)));
                parsed_code_size += instruction_size;
                mut_input = input;
            }
            842 => {
                //la
                parsed_code_size += 1;
                let (input, label_offset) = le_u16(input)?;
                code.push((parsed_code_size, Line::Label(label_offset)));
                parsed_code_size += 1;
                let (input, arity) = le_u16(input)?;
                code.push((parsed_code_size, Line::Arity(arity)));
                parsed_code_size += 1;
                mut_input = input;
            }
            843 => {
                //a
                parsed_code_size += 1;
                let (input, arity) = le_u16(input)?;
                code.push((parsed_code_size, Line::Arity(arity)));
                parsed_code_size += 1;
                mut_input = input;
            }
            i => panic!("Unknown instruction: {}", i),
        }
    }
    Ok((mut_input, code))
}

/// Parse the raw data. The data can probably end up in the ultimate llvm-ir, so
/// there is (as of yet) no reason to parse it any other way.
fn parse_data(input: &[u8], data_size: u32) -> IResult<&[u8], Vec<u64>> {
    // data_size specifies the number of 64-bit words that must be read
    count(le_u64, data_size as usize)(input)
}

/// Parse the entire symbol table.
fn parse_symbol_table(
    input: &[u8],
    nr_of_symbols: u32,
    _symbol_table_size: u32,
) -> IResult<&[u8], SymbolTable> {
    let (input, entries) = count(parse_symbol_table_entry, nr_of_symbols as usize)(input)?;
    Ok((input, entries))
}

/// Parse a single symbol table entry. Since we will eventually need to separate
/// the offset from the target, we already do that here. We also convert the
/// target integer into a more readable Segment type.
fn parse_symbol_table_entry(input: &[u8]) -> IResult<&[u8], SymbolTableEntry> {
    let (input, offset_and_target) = le_u32(input)?;
    let offset = offset_and_target >> 2;
    let target = offset_and_target & 0x00000001;
    let target = match target {
        0 => Segment::Code,
        1 => Segment::Data,
        _ => unreachable!(),
    };
    let (input, name) = parse_null_terminated_string(input)?;
    Ok((
        input,
        SymbolTableEntry {
            offset,
            target,
            name,
        },
    ))
}

/// The strings segment is only required for the 32-bit parser. See *Lazy
/// Interworking of Compiled and Interpreted Code for Sandboxing and Distributed
/// Systems* for an explanation as to why. Data from this parser is therefore
/// discarded where it is called.
fn parse_strings(input: &[u8], string_size: u32) -> IResult<&[u8], Vec<u32>> {
    count(le_u32, string_size as usize)(input)
}

/// Parse C-like strings. Entries in the symbol table are stored with
/// null-terminators. This function takes the string up to the null terminator
/// and returns it in a vector. The null terminator itself is discarded.
fn parse_null_terminated_string(input: &[u8]) -> IResult<&[u8], Vec<u8>> {
    let (input, str) = take_until("\0")(input)?;
    let (input, _) = take(1usize)(input)?;
    Ok((input, str.to_vec()))
}

/// Parse the entire program by first parsing the header and using the metadata
/// from that to parse the other sections. Data from the strings section is
/// completely discarded as we have no used for it on our 64-bit system.
pub(crate) fn parse_program(input: &[u8]) -> Result<ParsedProgram, String> {
    let (input, header) = parse_header(input).map_err(|err| err.to_string())?;
    let (input, code) = parse_code(input, header.code_size).map_err(|err| err.to_string())?;
    let (input, _) = parse_strings(input, header.strings_size).map_err(|err| err.to_string())?;
    let (input, data) = parse_data(input, header.data_size).map_err(|err| err.to_string())?;
    let (input, symbol_table) =
        parse_symbol_table(input, header.nr_of_symbols, header.symbol_table_size)
            .map_err(|err| err.to_string())?;
    let (input, code_relocations) =
        parse_relocations(input, header.code_reloc_size).map_err(|err| err.to_string())?;
    let (input, data_relocations) =
        parse_relocations(input, header.data_reloc_size).map_err(|err| err.to_string())?;

    // If we did not fully nom nom nom our input, this means some parsing went
    // wrong. It is better to not return anything in that case.
    if !input.is_empty() {
        return Err(String::from("Did not fully parse the input file."));
    }

    Ok(ParsedProgram {
        code,
        data,
        symbol_table,
        code_relocations,
        data_relocations,
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    #[test]
    fn parsing_hello_world_is_ok() {
        for file in fs::read_dir("tests/").unwrap() {
            let file = file.unwrap();
            let content = fs::read(file.path()).unwrap();
            assert!(parse_program(content.as_slice()).is_ok());
        }
    }

    #[test]
    fn parsing_empty_is_error() {
        assert!(parse_program(&[]).is_err());
    }
}
