use std::env;
use std::fs;
use std::string::String;

mod ast;
mod parse;
mod pass;

fn main() -> Result<(), String> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        return Err(String::from("Usage: llvm-code-generator file"));
    }
    let content = fs::read(&args[1]).expect("Could not read input file");
    let parsed_program = parse::parse_program(content.as_slice())?;
    let relocated_program = pass::relocation::relocate_program(parsed_program)?;
    eprintln!("relocated_program = {:#?}", relocated_program);
    Ok(())
}
