//! Everything related to the symbol table

pub(crate) type SymbolTable = Vec<SymbolTableEntry>;

#[derive(Debug)]
pub(crate) struct SymbolTableEntry {
    // Offsets in the bytecode format are actually only 30 bits. For easy of use
    // we store 32 bits instead, with the upper two bits set to 0.
    pub(crate) offset: u32,
    pub(crate) target: Segment,
    pub(crate) name: Vec<u8>,
}

#[derive(Debug)]
pub(crate) enum Segment {
    Code,
    Data,
}
