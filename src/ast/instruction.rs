//! This module contains a generalized instruction enum. Reason for this is that multiple passes
//! use the same instruction list. It also contains all functions related to the instruction
//! enumeration that are pass independent.

macro_rules! instructions {
    (enum $ename:ident {
        $($vname:ident ( $($vty: ty),* )),*
    }) => {
#[derive(Debug, Clone)]
#[allow(non_snake_case)]
#[allow(non_camel_case_types)]
/// An ADT of every supported ABC instruction. Also contains specific 32-bit
/// instructions which will be left here for completeness. Is not a 1-on-1
/// mapping with the C-enum that also considers the data hints to be
/// instructions.
        pub(crate) enum $ename<L> {
            $($vname ( $($vty),* )),*
        }

        impl<L> $ename<L> {
            pub(crate) fn len(&self) -> u32 {
                match self {
                    $($ename::$vname(..) => instructions!(@count ($($vty),*))),*
                }
            }
        }

    };

    (@count ()) => (1);
    (@count ($a:ty)) => (2);
    (@count ($a:ty, $b:ty)) => (3);
    (@count ($a:ty, $b:ty, $c:ty)) => (4);
    (@count ($a:ty, $b:ty, $c:ty, $d:ty)) => (5);
    (@count ($a:ty, $b:ty, $c:ty, $d:ty, $e:ty)) => (6);
    (@count ($a:ty, $b:ty, $c:ty, $d:ty, $e:ty, $f:ty)) => (7);
}

/// Offsets in the b or a stack
pub(crate) type StackIndex = i16;

pub(crate) type LabelOffset = u16;

pub(crate) type CAFLabelOffset = u16;

pub(crate) type StringOffset = u16;
pub(crate) type Integer = u64;
pub(crate) type Arity = u16;
pub(crate) type Char = u8;
pub(crate) type Real = f64;
/// A 32-bit vector, used for fill instructions
pub(crate) type BitVector = u32;

instructions! {
    enum Instruction {
        absR(),
        acosR(),
        addI(),
        addIo(),
        addLU(),
        addR(),
        add_empty_node2(L),
        add_empty_node3(L),
        add_empty_node4(L),
        add_empty_node5(L),
        add_empty_node6(L),
        add_empty_node7(L),
        add_empty_node8(L),
        add_empty_node9(L),
        add_empty_node10(L),
        add_empty_node11(L),
        add_empty_node12(L),
        add_empty_node13(L),
        add_empty_node14(L),
        add_empty_node15(L),
        add_empty_node16(L),
        add_empty_node17(L),
        add_empty_node18(L),
        add_empty_node19(L),
        add_empty_node20(L),
        add_empty_node21(L),
        add_empty_node22(L),
        add_empty_node23(L),
        add_empty_node24(L),
        add_empty_node25(L),
        add_empty_node26(L),
        add_empty_node27(L),
        add_empty_node28(L),
        add_empty_node29(L),
        add_empty_node30(L),
        add_empty_node31(L),
        add_empty_node32(L),
        andI(),
        asinR(),
        atanR(),
        build(StackIndex, L),
        build0(L),
        build1(L),
        build2(L),
        build3(L),
        build4(L),
        buildAC(StringOffset),
        buildh(StackIndex, L),
        buildh0(L),
        buildh1(L),
        buildh2(L),
        buildh3(L),
        buildh4(L),
        buildhr01(L),
        buildhr02(L),
        buildhr03(L),
        buildhr04(L),
        buildhr10(L),
        buildhr11(L),
        buildhr12(L),
        buildhr13(L),
        buildhr1b(StackIndex, L),
        buildhr20(L),
        buildhr21(L),
        buildhr22(L),
        buildhr30(L),
        buildhr31(L),
        buildhr40(L),
        build_node_rtn(StackIndex),
        build_node2_rtn(),
        build_r(StackIndex, StackIndex, L, StackIndex, StackIndex),
        build_ra0(StackIndex, StackIndex, L),
        build_ra1(StackIndex, StackIndex, L, StackIndex),
        build_r0b(StackIndex, StackIndex, L),
        buildBFALSE(),
        buildBTRUE(),
        buildB_b(StackIndex),
        buildC(Char),
        buildC_b(StackIndex),
        buildF_b(StackIndex),
        buildI(Integer),
        buildI_b(StackIndex),
        buildR(Real, StackIndex),
        buildR_b(StackIndex),
        buildhr(StackIndex, L, StackIndex),
        buildhra0(StackIndex, L),
        buildhra1(StackIndex, L),
        buildhr0b(StackIndex, L),
        build_r01(StackIndex, L),
        build_r02(StackIndex, L),
        build_r03(StackIndex, L),
        build_r04(StackIndex, L),
        build_r10(StackIndex, L),
        build_r11(StackIndex, StackIndex, L),
        build_r12(StackIndex, StackIndex, L),
        build_r13(StackIndex, StackIndex, L),
        build_r1b(StackIndex, StackIndex, L, StackIndex),
        build_r20(StackIndex, L),
        build_r21(StackIndex, StackIndex, L),
        build_r30(StackIndex, L),
        build_r31(StackIndex, StackIndex, L),
        build_r40(StackIndex, L),
        build_u(StackIndex, L, StackIndex),
        build_u01(L),
        build_u02(L),
        build_u03(L),
        build_u0b(StackIndex, L),
        build_u11(L),
        build_u12(L),
        build_u13(L),
        build_u1b(StackIndex, L),
        build_u21(L),
        build_u22(L),
        build_u2b(StackIndex, L),
        build_u31(L),
        build_ua1(StackIndex, L),
        catAC(),
        ccall(StringOffset, StringOffset),
        centry(),
        clzb(),
        cmpAC(),
        cosR(),
        create(),
        creates(StackIndex),
        create_array(),
        create_arrayBOOL(),
        create_arrayCHAR(),
        create_arrayINT(),
        create_arrayREAL(),
        create_array_(),
        create_array_BOOL(),
        create_array_CHAR(),
        create_array_INT(),
        create_array_REAL(),
        create_array_r(StackIndex, StackIndex, L),
        create_array_r_(StackIndex, StackIndex, L),
        create_array_r_a(StackIndex, L),
        create_array_r_b(StackIndex, L),
        decI(),
        divI(),
        divLU(),
        divR(),
        divU(),
        entierR(),
        eqAC(),
        eqAC_a(StringOffset),
        eqB(),
        eqB_aFALSE(StackIndex),
        eqB_aTRUE(StackIndex),
        eqB_bFALSE(StackIndex),
        eqB_bTRUE(StackIndex),
        eqC(),
        eqC_a(StackIndex, Char),
        eqC_b(StackIndex, Char),
        eqCc(Char),
        eqD_b(L),
        eq_desc(StackIndex, L),
        eq_desc_b(L),
        eq_nulldesc(StackIndex, L),
        eqI(),
        eqI_a(StackIndex, Integer),
        eqI_b(StackIndex, Integer),
        eqIi(Integer),
        eqR(),
        eqR_b(StackIndex, Real, StackIndex),
        expR(),
        fill(StackIndex, StackIndex, L),
        fill0(StackIndex, L),
        fill1_r0101(StackIndex),
        fill1_r0111(StackIndex, L),
        fill1_r02001(StackIndex),
        fill1_r02010(StackIndex),
        fill1_r02011(StackIndex),
        fill1_r02101(StackIndex, L),
        fill1_r02110(StackIndex, L),
        fill1_r02111(StackIndex, L),
        fill1_r11001(StackIndex),
        fill1_r11011(StackIndex),
        fill1_r11101(StackIndex, L),
        fill1_r11111(StackIndex, L),
        fill1_r20111(StackIndex, L),
        fill1(StackIndex, L),
        fill1001(StackIndex),
        fill1010(StackIndex),
        fill1011(StackIndex),
        fill1101(StackIndex, L),
        fill2(StackIndex, L),
        fill2a001(StackIndex, StackIndex),
        fill2a002(StackIndex, StackIndex, StackIndex),
        fill2a011(StackIndex, StackIndex),
        fill2a012(StackIndex, StackIndex),
        fill2ab011(StackIndex, StackIndex),
        fill2ab013(StackIndex, StackIndex, StackIndex, StackIndex),
        fill2ab002(StackIndex, StackIndex, StackIndex),
        fill2ab003(StackIndex, StackIndex, StackIndex, StackIndex),
        fill2b001(StackIndex, StackIndex),
        fill2b002(StackIndex, StackIndex, StackIndex),
        fill2b011(StackIndex, StackIndex),
        fill2b012(StackIndex, StackIndex, StackIndex),
        fill2_r00(StackIndex, StackIndex, BitVector, CAFLabelOffset),
        fill2_r01(StackIndex, StackIndex, BitVector, CAFLabelOffset),
        fill2_r10(StackIndex, L, StackIndex, BitVector, CAFLabelOffset),
        fill2_r11(StackIndex, L, StackIndex, BitVector, CAFLabelOffset),
        fill3(StackIndex, L),
        fill3a10(StackIndex, L),
        fill3a11(StackIndex, L, StackIndex),
        fill3a12(StackIndex, L, StackIndex, StackIndex),
        fill3aaab13(StackIndex, L, StackIndex, StackIndex, StackIndex),
        fill3_r(
            StackIndex,
            L,
            StackIndex,
            StackIndex,
            BitVector,
            CAFLabelOffset
        ),
        fill3_r01a(StackIndex, L, StackIndex),
        fill3_r01b(StackIndex, L, StackIndex),
        fill4(StackIndex, L),
        fillcaf(CAFLabelOffset, StackIndex, StackIndex),
        fillh(StackIndex, StackIndex, L),
        fillh0(StackIndex, L),
        fillh1(StackIndex, L),
        fillh2(StackIndex, L),
        fillh3(StackIndex, L),
        fillh4(StackIndex, L),
        fillB_b(StackIndex, StackIndex),
        fillC_b(StackIndex, StackIndex),
        fillF_b(StackIndex, StackIndex),
        fillI(Integer, StackIndex),
        fillI_b(StackIndex, StackIndex),
        fillR_b(StackIndex, StackIndex),
        fill_a(StackIndex, StackIndex),
        fill_r(
            StackIndex,
            StackIndex,
            StackIndex,
            L,
            StackIndex,
            StackIndex
        ),
        fill_r01(StackIndex, StackIndex, L),
        fill_r02(StackIndex, StackIndex, L),
        fill_r03(StackIndex, StackIndex, L),
        fill_r0b(StackIndex, StackIndex, L, StackIndex),
        fill_r10(StackIndex, StackIndex, L),
        fill_r11(StackIndex, StackIndex, StackIndex, L),
        fill_r12(StackIndex, StackIndex, StackIndex, L),
        fill_r13(StackIndex, StackIndex, StackIndex, L),
        fill_r1b(StackIndex, StackIndex, StackIndex, L, StackIndex),
        fill_r20(StackIndex, StackIndex, L),
        fill_r21(StackIndex, StackIndex, StackIndex, L),
        fill_r22(StackIndex, StackIndex, StackIndex, L),
        fill_r30(StackIndex, StackIndex, L),
        fill_r31(StackIndex, StackIndex, StackIndex, L),
        fill_r40(StackIndex, StackIndex, L),
        fill_ra0(StackIndex, StackIndex, StackIndex, L),
        fill_ra1(StackIndex, StackIndex, StackIndex, L, StackIndex),
        fill_u(StackIndex, StackIndex, L, StackIndex),
        get_desc_arity_offset(),
        get_node_arity(StackIndex),
        get_thunk_arity(),
        get_thunk_desc(),
        gtI(),
        halt(),
        incI(),
        instruction(Integer),
        is_record(StackIndex),
        jmp(L),
        jmpD_ab(L, L, L),
        jmpD_ae(L, L, L),
        jmpD_be(L, L, L),
        jmp_eval(),
        jmp_eval_upd(),
        jmp_false(L),
        jmp_i(StackIndex),
        jmp_i0(),
        jmp_i1(),
        jmp_i2(),
        jmp_i3(),
        jmp_true(L),
        jsr(L),
        jsr_eval(StackIndex),
        jsr_eval0(),
        jsr_eval1(),
        jsr_eval2(),
        jsr_eval3(),
        jsr_i(StackIndex),
        jsr_i0(),
        jsr_i1(),
        jsr_i2(),
        jsr_i3(),
        lnR(),
        load_i(Integer),
        load_module_name(),
        load_si16(Integer),
        load_si32(Integer),
        load_ui8(Integer),
        log10R(),
        ltC(),
        ltI(),
        ltR(),
        ltU(),
        mulI(),
        mulIo(),
        mulR(),
        mulUUL(),
        negI(),
        negR(),
        notB(),
        notI(),
        orI(),
        pop_a(StackIndex),
        pop_b(StackIndex),
        powR(),
        print(StringOffset),
        printD(),
        print_char(),
        print_int(),
        print_real(),
        print_string(),
        print_symbol_sc(StackIndex),
        pushcaf(StackIndex, StackIndex, CAFLabelOffset),
        pushcaf10(CAFLabelOffset),
        pushcaf11(CAFLabelOffset),
        pushcaf20(CAFLabelOffset),
        pushcaf31(CAFLabelOffset),
        pushA_a(StackIndex),
        pushBFALSE(),
        pushBTRUE(),
        pushB_a(StackIndex),
        pushB0_pop_a1(),
        pushC(Char),
        pushC_a(StackIndex),
        pushC0_pop_a1(),
        pushD(L),
        pushD_a(StackIndex),
        pushF_a(StackIndex),
        pushI(Integer),
        pushI_a(StackIndex),
        pushI0_pop_a1(),
        pushL(),
        pushLc(),
        pushR(Real, StackIndex),
        pushR_a(StackIndex),
        push_a(StackIndex),
        push_a_r_args(),
        push_arg(StackIndex, StackIndex),
        push_arg1(StackIndex),
        push_arg2(StackIndex),
        push_arg2l(StackIndex),
        push_arg3(StackIndex),
        push_arg4(StackIndex),
        push_arg_b(StackIndex),
        push_args(StackIndex, StackIndex),
        push_args1(StackIndex),
        push_args2(StackIndex),
        push_args3(StackIndex),
        push_args4(StackIndex),
        push_args_u(StackIndex, StackIndex, StackIndex),
        push_array(StackIndex),
        push_arraysize(),
        push_a_b(StackIndex),
        push_b(StackIndex),
        push_b_a(StackIndex),
        push_finalizers(),
        push_node(StackIndex, L),
        push_node0(L),
        push_node1(L),
        push_node2(L),
        push_node3(L),
        push_node4(L),
        push_node_(StackIndex),
        push_node_u(StackIndex, L, StackIndex),
        push_node_u01(L),
        push_node_u02(L),
        push_node_u03(L),
        push_node_u0b(StackIndex, L),
        push_node_u11(L),
        push_node_u12(L),
        push_node_u13(L),
        push_node_u1b(StackIndex, L),
        push_node_u21(L),
        push_node_u22(L),
        push_node_u31(L),
        push_node_ua1(StackIndex, L),
        push_r_arg_D(),
        push_r_arg_t(),
        push_r_args(StackIndex, StackIndex, StackIndex),
        push_r_args01(StackIndex),
        push_r_args02(StackIndex),
        push_r_args03(StackIndex),
        push_r_args04(StackIndex),
        push_r_args0b(StackIndex, StackIndex),
        push_r_args10(StackIndex),
        push_r_args11(StackIndex),
        push_r_args12(StackIndex),
        push_r_args13(StackIndex),
        push_r_args1b(StackIndex, StackIndex),
        push_r_args20(StackIndex),
        push_r_args21(StackIndex),
        push_r_args22(StackIndex),
        push_r_args30(StackIndex),
        push_r_args31(StackIndex),
        push_r_args40(StackIndex),
        push_r_argsa0(StackIndex, StackIndex),
        push_r_argsa1(StackIndex, StackIndex),
        push_r_args_a(StackIndex, StackIndex, StackIndex, StackIndex),
        push_r_args_a1(StackIndex),
        push_r_args_a2l(StackIndex),
        push_r_args_a3(StackIndex),
        push_r_args_a4(StackIndex),
        push_r_args_aa1(StackIndex, StackIndex),
        push_r_args_b(StackIndex, StackIndex, StackIndex),
        push_r_args_b0b11(StackIndex),
        push_r_args_b0221(StackIndex),
        push_r_args_b1(StackIndex, StackIndex),
        push_r_args_b1111(StackIndex),
        push_r_args_b2l1(StackIndex),
        push_r_args_b31(StackIndex),
        push_r_args_b41(StackIndex),
        push_r_args_b1l2(StackIndex),
        push_r_args_b2(StackIndex, StackIndex),
        push_r_args_b22(StackIndex),
        push_t_r_a(StackIndex),
        push_t_r_args(),
        replace(),
        replaceBOOL(),
        replaceCHAR(),
        replaceINT(),
        replaceREAL(),
        replace_r(StackIndex, StackIndex),
        repl_args(StackIndex),
        repl_args1(),
        repl_args2(),
        repl_args3(),
        repl_args4(),
        repl_args_b(),
        repl_r_args(StackIndex, StackIndex),
        repl_r_args01(),
        repl_r_args02(),
        repl_r_args03(),
        repl_r_args04(),
        repl_r_args0b(StackIndex),
        repl_r_args10(),
        repl_r_args11(),
        repl_r_args12(),
        repl_r_args13(),
        repl_r_args14(),
        repl_r_args1b(StackIndex),
        repl_r_args20(),
        repl_r_args21(),
        repl_r_args22(),
        repl_r_args23(),
        repl_r_args24(),
        repl_r_args2b(StackIndex),
        repl_r_args30(),
        repl_r_args31(),
        repl_r_args32(),
        repl_r_args33(),
        repl_r_args34(),
        repl_r_args3b(StackIndex),
        repl_r_args40(),
        repl_r_argsa0(StackIndex),
        repl_r_argsa1(StackIndex),
        repl_r_args_a(StackIndex, StackIndex, StackIndex),
        repl_r_args_aab11(),
        repl_r_args_a2021(),
        repl_r_args_a21(),
        repl_r_args_a31(),
        repl_r_args_a41(),
        repl_r_args_aa1(StackIndex),
        remI(),
        rtn(),
        select(),
        selectBOOL(),
        selectCHAR(),
        selectINT(),
        selectREAL(),
        select_r(StackIndex, StackIndex),
        select_r01(),
        select_r02(),
        select_r03(),
        select_r04(),
        select_r0b(StackIndex),
        select_r10(),
        select_r11(),
        select_r12(),
        select_r13(),
        select_r14(),
        select_r1b(StackIndex),
        select_r20(),
        select_r21(),
        select_r22(),
        select_r23(),
        select_r24(),
        select_r2b(StackIndex),
        set_finalizers(),
        shiftlI(),
        shiftrI(),
        shiftrU(),
        sinR(),
        sliceAC(),
        subI(),
        subIo(),
        subLU(),
        subR(),
        sqrtR(),
        tanR(),
        testcaf(CAFLabelOffset),
        update(),
        updateAC(),
        updateBOOL(),
        updateCHAR(),
        updateINT(),
        updateREAL(),
        updatepop_a(StackIndex, StackIndex),
        updatepop_b(StackIndex, StackIndex),
        update_a(StackIndex, StackIndex),
        update_b(StackIndex, StackIndex),
        update_r(StackIndex, StackIndex),
        update_r01(),
        update_r02(),
        update_r03(),
        update_r04(),
        update_r0b(StackIndex),
        update_r10(),
        update_r11(),
        update_r12(),
        update_r13(),
        update_r14(),
        update_r1b(StackIndex),
        update_r20(),
        update_r21(),
        update_r22(),
        update_r23(),
        update_r24(),
        update_r2b(StackIndex),
        update_r30(),
        update_r31(),
        update_r32(),
        update_r33(),
        update_r34(),
        update_r3b(StackIndex),
        xorI(),
        BtoAC(),
        CtoAC(),
        ItoAC(),
        ItoC(),
        ItoR(),
        RtoAC(),
        RtoI(),
        jmp_ap1(),
        jsr_ap1(),
        jmp_ap2(),
        jsr_ap2(),
        jmp_ap3(),
        jsr_ap3(),
        jmp_ap4(),
        jsr_ap4(),
        jmp_ap5(),
        jsr_ap5(),
        jmp_ap6(),
        jsr_ap6(),
        jmp_ap7(),
        jsr_ap7(),
        jmp_ap8(),
        jsr_ap8(),
        jmp_ap9(),
        jsr_ap9(),
        jmp_ap10(),
        jsr_ap10(),
        jmp_ap11(),
        jsr_ap11(),
        jmp_ap12(),
        jsr_ap12(),
        jmp_ap13(),
        jsr_ap13(),
        jmp_ap14(),
        jsr_ap14(),
        jmp_ap15(),
        jsr_ap15(),
        jmp_ap16(),
        jsr_ap16(),
        jmp_ap17(),
        jsr_ap17(),
        jmp_ap18(),
        jsr_ap18(),
        jmp_ap19(),
        jsr_ap19(),
        jmp_ap20(),
        jsr_ap20(),
        jmp_ap21(),
        jsr_ap21(),
        jmp_ap22(),
        jsr_ap22(),
        jmp_ap23(),
        jsr_ap23(),
        jmp_ap24(),
        jsr_ap24(),
        jmp_ap25(),
        jsr_ap25(),
        jmp_ap26(),
        jsr_ap26(),
        jmp_ap27(),
        jsr_ap27(),
        jmp_ap28(),
        jsr_ap28(),
        jmp_ap29(),
        jsr_ap29(),
        jmp_ap30(),
        jsr_ap30(),
        jmp_ap31(),
        jsr_ap31(),
        jmp_ap32(),
        jsr_ap32(),
        add_arg(),
        add_arg0(),
        add_arg1(),
        add_arg2(),
        add_arg3(),
        add_arg4(),
        add_arg5(),
        add_arg6(),
        add_arg7(),
        add_arg8(),
        add_arg9(),
        add_arg10(),
        add_arg11(),
        add_arg12(),
        add_arg13(),
        add_arg14(),
        add_arg15(),
        add_arg16(),
        add_arg17(),
        add_arg18(),
        add_arg19(),
        add_arg20(),
        add_arg21(),
        add_arg22(),
        add_arg23(),
        add_arg24(),
        add_arg25(),
        add_arg26(),
        add_arg27(),
        add_arg28(),
        add_arg29(),
        add_arg30(),
        add_arg31(),
        add_arg32(),
        eval_upd0(),
        eval_upd1(),
        eval_upd2(),
        eval_upd3(),
        eval_upd4(),
        eval_upd5(),
        eval_upd6(),
        eval_upd7(),
        eval_upd8(),
        eval_upd9(),
        eval_upd10(),
        eval_upd11(),
        eval_upd12(),
        eval_upd13(),
        eval_upd14(),
        eval_upd15(),
        eval_upd16(),
        eval_upd17(),
        eval_upd18(),
        eval_upd19(),
        eval_upd20(),
        eval_upd21(),
        eval_upd22(),
        eval_upd23(),
        eval_upd24(),
        eval_upd25(),
        eval_upd26(),
        eval_upd27(),
        eval_upd28(),
        eval_upd29(),
        eval_upd30(),
        eval_upd31(),
        eval_upd32(),
        fill_a01_pop_rtn(),
        swap_a1(),
        swap_a2(),
        swap_a3(),
        swap_a(StackIndex),

        closeF(),
        endF(),
        endSF(),
        errorF(),
        flushF(),
        openF(),
        openSF(),
        positionF(),
        positionSF(),
        readFC(),
        readFI(),
        readFR(),
        readFS(),
        readFString(),
        readLineF(),
        readLineSF(),
        readSFC(),
        readSFI(),
        readSFR(),
        readSFS(),
        reopenF(),
        seekF(),
        seekSF(),
        shareF(),
        stderrF(),
        stdioF(),
        writeFC(),
        writeFI(),
        writeFR(),
        writeFS(),
        writeFString(),

        addIi(Integer),
        andIi(Integer),
        andIio(StackIndex, Integer),
        buildh0_dup_a(L, StackIndex),
        buildh0_dup2_a(L, StackIndex),
        buildh0_dup3_a(L, StackIndex),
        buildh0_put_a(L, StackIndex),
        buildh0_put_a_jsr(L, StackIndex, L),
        buildho2(StackIndex, StackIndex, L),
        buildo1(StackIndex, L),
        buildo2(StackIndex, StackIndex, L),
        dup_a(StackIndex),
        dup2_a(StackIndex),
        dup3_a(StackIndex),
        exchange_a(StackIndex, StackIndex),
        geC(),
        jmp_b_false(StackIndex, L),
        jmp_eqACio(StackIndex, StringOffset, L),
        jmp_eqC_b(StackIndex, Char, L),
        jmp_eqC_b2(StackIndex, Char, L, Char, L),
        jmp_eqCc(Char, L),
        jmp_eqD_b(L, L),
        jmp_eqD_b2(L, L, L, L),
        jmp_eqI(L),
        jmp_eqI_b(StackIndex, Integer, L),
        jmp_eqI_b2(StackIndex, Integer, L, Integer, L),
        jmp_eqIi(Integer, L),
        jmp_eq_desc(StackIndex, L, L),
        jmp_geI(L),
        jmp_ltI(L),
        jmp_neC_b(StackIndex, Char, L),
        jmp_neCc(Char, L),
        jmp_neI(L),
        jmp_neI_b(StackIndex, Integer, L),
        jmp_neIi(Integer, L),
        jmp_ne_desc(StackIndex, L, L),
        jmp_o_geI(StackIndex, L),
        jmp_o_geI_arraysize_a(StackIndex, StackIndex, L),
        ltIi(Integer),
        neI(),
        swap_b1(),
        pop_a_jmp(StackIndex, L),
        pop_a_jsr(StackIndex, L),
        pop_a_rtn(StackIndex),
        pop_ab_rtn(StackIndex, StackIndex),
        pop_b_jmp(StackIndex, L),
        pop_b_jsr(StackIndex, L),
        pop_b_pushBFALSE(StackIndex),
        pop_b_pushBTRUE(StackIndex),
        pop_b_rtn(StackIndex),
        pushD_a_jmp_eqD_b2(StackIndex, L, L, L, L),
        push_a_jsr(StackIndex, L),
        push_b_decI(StackIndex),
        push_b_incI(StackIndex),
        push_b_jsr(StackIndex, L),
        push_arraysize_a(StackIndex),
        push_jsr_eval(StackIndex),
        push_a2(StackIndex, StackIndex),
        push_ab(StackIndex, StackIndex),
        push_b2(StackIndex, StackIndex),
        push2_a(StackIndex),
        push2_b(StackIndex),
        push3_a(StackIndex),
        push3_b(StackIndex),
        push_update_a(StackIndex, StackIndex),
        put_a(StackIndex),
        put_a_jmp(StackIndex, L),
        put_b(StackIndex),
        put_b_jmp(StackIndex, L),
        selectBOOLoo(StackIndex, StackIndex),
        selectCHARoo(StackIndex, StackIndex),
        selectINToo(StackIndex, StackIndex),
        selectREALoo(StackIndex, StackIndex),
        selectoo(StackIndex, StackIndex),
        update2_a(StackIndex, StackIndex),
        update2_b(StackIndex, StackIndex),
        update2pop_a(StackIndex, StackIndex),
        update2pop_b(StackIndex, StackIndex),
        update3_a(StackIndex, StackIndex),
        update3_b(StackIndex, StackIndex),
        update3pop_a(StackIndex, StackIndex),
        update3pop_b(StackIndex, StackIndex),
        update4_a(StackIndex, StackIndex),
        updates2_a(StackIndex, StackIndex, StackIndex),
        updates2_a_pop_a(StackIndex, StackIndex, StackIndex, StackIndex),
        updates2_b(StackIndex, StackIndex, StackIndex),
        updates2pop_a(StackIndex, StackIndex, StackIndex),
        updates2pop_b(StackIndex, StackIndex, StackIndex),
        updates3_a(StackIndex, StackIndex, StackIndex, StackIndex),
        updates3_b(StackIndex, StackIndex, StackIndex, StackIndex),
        updates3pop_a(StackIndex, StackIndex, StackIndex, StackIndex),
        updates3pop_b(StackIndex, StackIndex, StackIndex, StackIndex),
        updates4_a(StackIndex, StackIndex, StackIndex, StackIndex, StackIndex),

        absR_32(),
        acosR_32(),
        addR_32(),
        asinR_32(),
        atanR_32(),
        buildR_32(Real, L),
        buildR_b_32(StackIndex),
        cosR_32(),
        create_arrayREAL_32(),
        create_array_REAL_32(),
        divR_32(),
        entierR_32(),
        eqR_32(),
        eqR_b_32(StackIndex, Real, L),
        expR_32(),
        fillR_b_32(StackIndex, StackIndex),
        lnR_32(),
        log10R_32(),
        ltR_32(),
        mulR_32(),
        negR_32(),
        powR_32(),
        pushR_32(Real, L),
        pushR_a_32(StackIndex),
        replaceREAL_32(),
        selectREAL_32(),
        sinR_32(),
        sqrtR_32(),
        subR_32(),
        tanR_32(),
        updateREAL_32(),
        ItoR_32(),
        RtoAC_32(),
        RtoI_32(),
        print_real_32(),
        selectREALoo_32(StackIndex, StackIndex),
        readFR_32(),
        writeFR_32(),
        readSFR_32()
    }
}
