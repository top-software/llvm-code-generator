//! For now the only common part of the different pass ASTs are the instructions, in the future
//! multiple modules might be exported here.

pub(crate) mod instruction;
pub(crate) mod symbol_table;
